import React, { useState, useEffect, createContext } from "react";
import {
  NavigationContainer,
  DefaultTheme as NavigationDefaultTheme,
  DarkTheme as NavigationDarkTheme,
} from "@react-navigation/native";
import {
  DefaultTheme as PaperDefaultTheme,
  DarkTheme as PaperDarkTheme,
  Provider,
} from "react-native-paper";
import DrawerNavigation from "./app/navigation/DrawerNavigation";
import LoginStack from "./app/navigation/LoginStack";
import ThemeContext from "./app/Context/ThemeContext";

const CustomDefaultTheme = {
  ...NavigationDefaultTheme,
  ...PaperDefaultTheme,
  colors: {
    ...NavigationDefaultTheme.colors,
    ...PaperDefaultTheme.colors,
    background: "#ffffff",
    text: "#333333",
    icon: "rgba(28, 28, 30, 0.68)",
    heartIcon: "#eb290e",
  },
};

const CustomDarkTheme = {
  ...NavigationDarkTheme,
  ...PaperDarkTheme,
  colors: {
    ...NavigationDarkTheme.colors,
    ...PaperDarkTheme.colors,
    background: "#1e1e1e",
    text: "#ffffff",
    icon: "rgba(255, 255, 255, 0.68)",
    heartIcon: "#c8402d",
  },
};

export default function App() {
  const [userLogged, setUserLogged] = useState(true);
  const [isDarkTheme, setIsDarkTheme] = useState(false);

  const theme = isDarkTheme ? CustomDarkTheme : CustomDefaultTheme;

  return (
    <Provider theme={theme}>
      <ThemeContext.Provider value={setIsDarkTheme}>
        <NavigationContainer theme={theme}>
          {userLogged ? <DrawerNavigation /> : <LoginStack />}
        </NavigationContainer>
      </ThemeContext.Provider>
    </Provider>
  );
}
