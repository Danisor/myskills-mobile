import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";
import { Card, Avatar, useTheme } from "react-native-paper";
import { Icon } from "react-native-elements";

export default function ListPost({ navigation }) {
  const publications = [
    {
      user: "Daniel",
      profilePicture:
        "https://www.cinemascomics.com/wp-content/uploads/2016/11/Flash-liga-de-la-justicia-justice-league-trailer-e1485419301433.jpg",
      image:
        "https://lh3.googleusercontent.com/proxy/VE-p-GH6al3vSJVMe6T0XkGL9Zc_EQaFiTxrM9v3CiHq4csbnG-zX_UMvS10wSX4WKLKDd0S4eiL42zp16wOTlQS_Q3fb4cFkh2JZp1aGYUgQs2_SPbbLiIHGFgW8wJu6iOqzJZWwikXCkYqCQ-F_BKajKpYq-umMd02aLwKD25baBRS37rUh5UPYxDNCc28cc-J1r0IcwaHs-5hopoYBBMyC3-9CDiWX8nI",
      comments: [
        {
          comment: "Fullmetal alchemist the best anime in the all history",
          id: 222,
        },
        {
          comment: "Mustang is op",
          id: 222,
        },
      ],
      likes: 45,
    },
    {
      user: "Ariel",
      profilePicture:
        "https://vignette.wikia.nocookie.net/disney/images/a/a0/Ariel-1.png/revision/latest/top-crop/width/360/height/450?cb=20170929163212&path-prefix=es",
      image:
        "https://pm1.narvii.com/6360/4c8a8216ca3c2ae2dcbfc5023d2bcc86ad670981_00.jpg",
      comments: [
        {
          comment: "Saber of fate stay night",
          id: 111,
        },
      ],
      likes: 15,
    },
    {
      user: "Luis Alberto",
      profilePicture:
        "https://www.cinemascomics.com/wp-content/uploads/2020/07/spider-man-batman-marve-dc-960x720.jpg",
      image:
        "https://d1whtlypfis84e.cloudfront.net/guides/wp-content/uploads/2019/08/03091106/Trees-1024x682.jpg",
      comments: [
        {
          comment:
            "La mecánica cuántica es la rama de la física que estudia la naturaleza a escalas espaciales pequeñas, los sistemas atómicos y subatómicos y sus interacciones con la radiación electromagnética, en términos de cantidades observables. Se basa en la observación de que todas las formas de energía se liberan en unidades discretas o paquetes llamados cuantos.",
          id: 333,
        },
      ],
      likes: 4,
    },
  ];

  return (
    <View>
      <FlatList
        data={publications}
        renderItem={(post) => <Post post={post} navigation={navigation} />}
        keyExtractor={(item, index) => index.toString()}
        ListFooterComponent={<FooterList />}
      />
    </View>
  );
}

function Post(props) {
  const [more, setMore] = useState(null);
  const theme = useTheme();
  const { post, navigation } = props;
  const { user, profilePicture, image, comments, likes } = post.item;
  const firstComment = comments[0].comment;
  const [isLike, setIsLike] = useState(false);
  const [isSave, setIsSave] = useState(false);

  const avatar = () => {
    return <Avatar.Image size={40} source={{ uri: profilePicture }} />;
  };

  useEffect(() => {
    firstComment.length > 50 && setMore(true);
  }, []);

  return (
    <Card>
      <Card.Title title={user} left={avatar} />
      <Card.Cover source={{ uri: image }} />
      <Card.Actions style={styles.buttonView}>
        <View style={styles.leftButton}>
          <Icon
            type="material-community"
            name={isLike ? "heart" : "heart-outline"}
            containerStyle={styles.iconContainer}
            color={isLike ? theme.colors.heartIcon : theme.colors.icon}
            size={30}
            onPress={() => setIsLike(!isLike)}
          />
          <Icon
            type="material-community"
            name={"comment-outline"}
            color={theme.colors.icon}
            size={30}
            onPress={() => navigation.navigate("comments")}
          />
        </View>
        <View>
          <Icon
            type="material-community"
            name={isSave ? "bookmark" : "bookmark-outline"}
            color={theme.colors.icon}
            size={30}
            onPress={() => setIsSave(!isSave)}
          />
        </View>
      </Card.Actions>
      <Card.Actions
        style={{ flexDirection: "column", alignItems: "flex-start" }}
      >
        {/* likes  */}
        <Text
          style={[styles.textlikes, { color: theme.colors.text }]}
          onPress={() => navigation.navigate("likes")}
        >
          {likes} likes
        </Text>

        {/* comment  */}

        {more ? (
          <Text style={{ color: theme.colors.text }}>
            <Text style={{ fontWeight: "bold" }}>{user} </Text>
            {firstComment.substring(0, 50)}
            <Text style={styles.allComents} onPress={() => setMore(false)}>
              {" "}
              ...more
            </Text>
          </Text>
        ) : (
          <Text style={{ color: theme.colors.text }}>
            <Text style={{ fontWeight: "bold" }}>{user} </Text>
            {firstComment}
          </Text>
        )}

        <Text
          style={styles.allComents}
          onPress={() => navigation.navigate("comments")}
        >
          View all {comments.length} comments
        </Text>
      </Card.Actions>
    </Card>
  );
}

function FooterList(theme) {
  return (
    <View style={styles.notfoundPost}>
      <Text style={styles.allComents}> Not found posts</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonView: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: -15,
  },
  leftButton: {
    flexDirection: "row",
  },
  iconContainer: {
    marginRight: 15,
  },
  textlikes: {
    fontWeight: "bold",
  },
  allComents: {
    color: "#989992",
  },
  notfoundPost: {
    marginTop: 10,
    marginBottom: 20,
    alignItems: "center",
  },
});
