import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Icon } from "react-native-elements";
import { useTheme } from "@react-navigation/native";

export default function Header({ title, navigation }) {
  const theme = useTheme();
  return (
    <View style={styles.headerContainer}>
      {title === "Account" ? (
        <Icon
          type="material-community"
          name="arrow-left"
          size={28}
          color={theme.colors.icon}
          onPress={() => navigation.goBack()}
        />
      ) : (
        <Icon
          type="material-community"
          name="menu"
          size={28}
          color={theme.colors.icon}
          onPress={() => navigation.openDrawer()}
        />
      )}
      <View>
        <Text style={[styles.headerText, { color: theme.colors.text }]}>
          {title}
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
  },
  headerText: {
    fontSize: 20,
    fontWeight: "bold",
    letterSpacing: 1,
    marginLeft: 20,
  },
});
