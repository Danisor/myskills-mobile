import React, { useContext } from "react";
import { StyleSheet, View, Text, StatusBar } from "react-native";
import { useTheme } from "@react-navigation/native";
import { Switch, Drawer } from "react-native-paper";
import { Icon } from "react-native-elements";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import ThemeContext from "../Context/ThemeContext";

export default function CustomDrawer({ props }) {
  const theme = useTheme();
  const setIsDark = useContext(ThemeContext);
  return (
    <View style={{ flex: 1 }}>
      <DrawerContentScrollView {...props}>
        {/* <DrawerItemList {...props} />   if i want to show the Screens in the drawer*/}
        <Drawer.Section>
          <DrawerItem
            label="Account"
            labelStyle={{ color: theme.colors.text }}
            onPress={() => props.navigation.navigate("Account")}
            icon={() => (
              <Icon
                type="material-community"
                name="account-outline"
                color={theme.colors.icon}
              />
            )}
          />
          <DrawerItem
            label="Cerrar sesión"
            labelStyle={{ color: theme.colors.text }}
            onPress={() => alert("Cerrando...")}
            icon={() => (
              <Icon
                type="material-community"
                name="exit-to-app"
                color={theme.colors.icon}
              />
            )}
          />
        </Drawer.Section>
        <Drawer.Section title="preference">
          <View style={styles.preference}>
            <Icon
              type="material-community"
              name="brightness-4"
              color={theme.colors.icon}
            />
            <Text style={{ color: theme.colors.text }}>Dark mode</Text>
            <Switch
              value={theme.dark}
              onValueChange={(val) => setIsDark(val)}
              color="#c6a824"
            />
          </View>
        </Drawer.Section>
      </DrawerContentScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  preference: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    marginTop: 15,
    marginBottom: 15,
  },
});
