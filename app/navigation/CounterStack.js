import React from "react";
import { View, Text } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import Counter from "../Screens/Counter";
import Header from "../components/Header";

export default function CounterStack({ navigation }) {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="counter"
        component={Counter}
        options={{
          headerTitle: () => <Header title="Counter" navigation={navigation} />,
        }}
      />
    </Stack.Navigator>
  );
}
