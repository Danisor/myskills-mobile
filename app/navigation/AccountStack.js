import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Account from "../Screens/Account";
import Header from "../components/Header";

export default function CommunityStack({ navigation }) {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="account"
        component={Account}
        options={{
          headerTitle: () => <Header title="Account" navigation={navigation} />,
        }}
      />
    </Stack.Navigator>
  );
}
