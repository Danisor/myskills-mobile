import React from "react";
import { View, Text } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import Nutrition from "../Screens/Nutrition";
import Header from "../components/Header";

export default function NutritionStack({ navigation }) {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="nutrition"
        component={Nutrition}
        options={{
          headerTitle: () => (
            <Header title="Nutrition" navigation={navigation} />
          ),
        }}
      />
    </Stack.Navigator>
  );
}
