import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import TabNavigation from "./TabNavigation";
import { Icon } from "react-native-elements";
import CustomDrawer from "../components/CustomDrawer";
import AccountStack from "../navigation/AccountStack";

const Drawer = createDrawerNavigator();

export default function DrawerNavigation() {
  return (
    <Drawer.Navigator
      initialRouteName="Home"
      drawerContent={(props) => <CustomDrawer props={props} />}
    >
      <Drawer.Screen name="tabNavigation" component={TabNavigation} />
      <Drawer.Screen name="Account" component={AccountStack} />
    </Drawer.Navigator>
  );
}
