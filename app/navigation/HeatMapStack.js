import React from "react";
import { View, Text } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import HeatMap from "../Screens/HeatMap";
import Header from "../components/Header";

export default function HeatMapStack({ navigation }) {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="heatmap"
        component={HeatMap}
        options={{
          headerTitle: () => (
            <Header title="Heat Map" navigation={navigation} />
          ),
        }}
      />
    </Stack.Navigator>
  );
}
