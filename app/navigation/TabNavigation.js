import React from "react";
import { View, Text } from "react-native";
import { Icon } from "react-native-elements";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import CounterStack from "./CounterStack";
import CommunityStack from "./CommunityStack";
import HeatMapStack from "./HeatMapStack";
import NutritionStack from "./NutritionStack";
import InjuresStack from "./InjuresStack";

export default function TabNavigation() {
  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
      initialRouteName="counter"
      tabBarOptions={{
        inactiveTintColor: "#646464",
        activeTintColor: "#c6a824",
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => screenOptions(route, color),
      })}
    >
      <Tab.Screen
        name="counter"
        component={CounterStack}
        options={{ title: "Counter" }}
      />
      <Tab.Screen
        name="community"
        component={CommunityStack}
        options={{ title: "Community" }}
      />
      <Tab.Screen
        name="heatmap"
        component={HeatMapStack}
        options={{ title: "Heat Map" }}
      />
      <Tab.Screen
        name="nutrition"
        component={NutritionStack}
        options={{ title: "Nutrition" }}
      />
      <Tab.Screen
        name="injures"
        component={InjuresStack}
        options={{ title: "Injures" }}
      />
    </Tab.Navigator>
  );
}

const screenOptions = (route, color) => {
  let iconName;
  let iconType;
  switch (route.name) {
    case "counter":
      iconName = "unfold-more-vertical";
      iconType = "material-community";
      break;
    case "community":
      iconName = "account-group";
      iconType = "material-community";
      break;
    case "heatmap":
      iconName = "map-marker-radius";
      iconType = "material-community";
      break;
    case "nutrition":
      iconName = "carrot";
      iconType = "font-awesome-5";
      break;
    case "injures":
      iconName = "user-md";
      iconType = "font-awesome-5";
      break;
    default:
      break;
  }
  return <Icon type={iconType} name={iconName} size={22} color={color} />;
};
