import React from "react";
import { View, Text } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import Injures from "../Screens/Injures";
import Header from "../components/Header";

export default function InjuresStack({ navigation }) {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="injures"
        component={Injures}
        options={{
          headerTitle: () => <Header title="Injures" navigation={navigation} />,
        }}
      />
    </Stack.Navigator>
  );
}
