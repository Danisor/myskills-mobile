import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Community from "../Screens/community/Community";
import Header from "../components/Header";
import Profile from "../Screens/community/Profile";
import SearchCommunity from "../Screens/community/SearchCommunity";
import Comments from "../Screens/community/Comments";
import Likes from "../Screens/community/Likes";

export default function CommunityStack({ navigation }) {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="community"
        component={Community}
        options={{
          headerTitle: () => (
            <Header title="My Skills Community" navigation={navigation} />
          ),
        }}
      />
      <Stack.Screen
        name="comments"
        component={Comments}
        options={{ title: "Comments" }}
      />
      <Stack.Screen
        name="likes"
        component={Likes}
        options={{ title: "Likes" }}
      />
    </Stack.Navigator>
  );
}
