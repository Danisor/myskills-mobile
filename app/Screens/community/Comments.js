import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  SafeAreaView,
  ScrollView,
} from "react-native";
import { List, Divider, Avatar, useTheme } from "react-native-paper";
import { Icon } from "react-native-elements";
// we need the user id for search the image profile and the username
const data = {
  commentPost: {
    text: "building the UI coments",
    profilePicture:
      "https://www.cinemascomics.com/wp-content/uploads/2020/07/spider-man-batman-marve-dc-960x720.jpg",
    userName: "Luis Alberto",
  },
  comments: [
    {
      principal: "React native a wonderful technology for apps",
      id: 225,
      profilePicture:
        "https://www.cinemascomics.com/wp-content/uploads/2016/11/Flash-liga-de-la-justicia-justice-league-trailer-e1485419301433.jpg",
      userName: "Daniel",
      principalLikes: 14,
      replies: [
        {
          reply: "React native is easy to learn",
          idReply: 541,
          profilePictureReply:
            "https://www.cinemascomics.com/wp-content/uploads/2020/07/spider-man-batman-marve-dc-960x720.jpg",
          userNameReply: "Luis Alberto",
          likes: 2,
        },
        {
          reply: "I think android native is better",
          idReply: 103,
          profilePictureReply:
            "https://vignette.wikia.nocookie.net/disney/images/a/a0/Ariel-1.png/revision/latest/top-crop/width/360/height/450?cb=20170929163212&path-prefix=es",
          userNameReply: "Ariel",
          likes: 2,
        },
      ],
    },
    {
      principal:
        "The Great Barrier Reef is the world's largest coral reef system",
      id: 225,
      profilePicture:
        "https://www.cinemascomics.com/wp-content/uploads/2020/07/spider-man-batman-marve-dc-960x720.jpg",
      userName: "Luis Alberto",
      principalLikes: 2,
      replies: [
        {
          reply: "I want to know australia an the reef",
          idReply: 541,
          profilePictureReply:
            "https://www.cinemascomics.com/wp-content/uploads/2016/11/Flash-liga-de-la-justicia-justice-league-trailer-e1485419301433.jpg",
          userNameReply: "Daniel",
          likes: 1,
        },
      ],
    },
    {
      principal: "this pizza is so nice",
      profilePicture:
        "https://vignette.wikia.nocookie.net/disney/images/a/a0/Ariel-1.png/revision/latest/top-crop/width/360/height/450?cb=20170929163212&path-prefix=es",
      userName: "Ariel",
      id: 225,
      principalLikes: 4,
      replies: [
        {
          reply: "hooo im starving",
          idReply: 541,
          profilePictureReply:
            "https://www.cinemascomics.com/wp-content/uploads/2020/07/spider-man-batman-marve-dc-960x720.jpg",
          userNameReply: "Luis Alberto",
          likes: 2,
        },
        {
          reply: "do yo have the telephone number?",
          idReply: 541,
          profilePictureReply:
            "https://www.cinemascomics.com/wp-content/uploads/2020/07/spider-man-batman-marve-dc-960x720.jpg",
          userNameReply: "Luis Alberto",
          likes: 2,
        },
        {
          reply: "300124567",
          idReply: 541,
          profilePictureReply:
            "https://vignette.wikia.nocookie.net/disney/images/a/a0/Ariel-1.png/revision/latest/top-crop/width/360/height/450?cb=20170929163212&path-prefix=es",
          userNameReply: "Ariel",
          likes: 2,
        },
      ],
    },
  ],
};
export default function Comments() {
  const theme = useTheme();
  return (
    <ScrollView>
      <List.Item
        title={data.commentPost.userName}
        titleStyle={{ fontWeight: "bold" }}
        description={data.commentPost.text}
        descriptionStyle={{ color: theme.colors.text }}
        left={() => (
          <Avatar.Image
            size={55}
            source={{ uri: data.commentPost.profilePicture }}
            style={styles.avatar}
          />
        )}
      />
      <Divider />
      <FlatList
        data={data.comments}
        renderItem={(comment) => (
          <RenderComments comment={comment} theme={theme} />
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </ScrollView>
  );
}

function RenderComments(props) {
  const {
    principal,
    profilePicture,
    userName,
    principalLikes,
    replies,
  } = props.comment.item;

  const { theme } = props;
  const [viewReplies, setViewReplies] = useState(false);
  const [isLike, setIsLike] = useState(false);

  return (
    <View>
      <View>
        <List.Item
          title={userName}
          titleStyle={{ fontWeight: "bold" }}
          key={props.comment.index}
          description={principal}
          descriptionStyle={{ color: theme.colors.text }}
          left={() => (
            <Avatar.Image
              size={55}
              source={{ uri: profilePicture }}
              style={styles.avatar}
            />
          )}
          right={() => (
            <Icon
              type="material-community"
              name={isLike ? "heart" : "heart-outline"}
              size={15}
              containerStyle={{ marginTop: 15 }}
              color={isLike ? theme.colors.heartIcon : theme.colors.icon}
              onPress={() => setIsLike(!isLike)}
            />
          )}
        />
      </View>
      <View style={styles.buttons}>
        <Text style={{ color: theme.colors.icon, fontSize: 12 }}>
          {principalLikes} likes {"   "}
          <Text>Reply {"   "}</Text>
          {viewReplies ? (
            <Text onPress={() => setViewReplies(false)}>Hide replies...</Text>
          ) : (
            <Text onPress={() => setViewReplies(true)}>
              View {replies.length} replies...
            </Text>
          )}
        </Text>

        {viewReplies &&
          replies.map((info, index) => (
            <List.Item
              title={info.userNameReply}
              titleStyle={{ fontWeight: "bold" }}
              key={index}
              description={info.reply}
              descriptionStyle={{ color: theme.colors.text }}
              style={{ marginTop: 5, marginBottom: -10 }}
              left={() => (
                <Avatar.Image
                  size={40}
                  source={{ uri: info.profilePictureReply }}
                  style={styles.avatar}
                />
              )}
            />
          ))}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  avatar: {
    marginRight: 10,
  },
  nameText: {
    fontWeight: "bold",
  },
  buttons: {
    marginLeft: 80,
    marginTop: -10,
    marginBottom: 20,
  },
});
