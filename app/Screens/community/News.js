import React from "react";
import ListPost from "../../components/ListPost";

export default function News({ navigation }) {
  return <ListPost navigation={navigation} />;
}
