import React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { useTheme } from "@react-navigation/native";
import { Icon } from "react-native-elements";
import News from "./News";
import Profile from "./Profile";
import SearchCommunity from "./SearchCommunity";
import CreatePost from "./CreatePost";

export default function Community({ navigation }) {
  // top tab navigator
  const Tab = createMaterialTopTabNavigator();
  const theme = useTheme();

  return (
    <Tab.Navigator
      tabBarOptions={{
        showIcon: true,
        showLabel: false,
        activeTintColor: theme.colors.texx,
        inactiveTintColor: theme.colors.icon,
        indicatorStyle: { backgroundColor: "#c6a824" },
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => screenOptions(route, color),
      })}
    >
      <Tab.Screen name="news" component={News} />
      <Tab.Screen name="profile" component={Profile} />
      <Tab.Screen name="createPost" component={CreatePost} />
      <Tab.Screen name="search" component={SearchCommunity} />
    </Tab.Navigator>
  );
}

const screenOptions = (route, color) => {
  let iconName;
  switch (route.name) {
    case "news":
      iconName = "home-outline";
      break;
    case "profile":
      iconName = "account-circle-outline";
      break;
    case "createPost":
      iconName = "plus-circle-outline";
      break;
    case "search":
      iconName = "magnify";
      break;
  }
  return <Icon type="material-community" name={iconName} color={color} />;
};
