import React from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { Searchbar, List, Avatar } from "react-native-paper";

export default function SearchCommunity() {
  const data = [
    {
      user: "Luis Alberto",
      profilePicture:
        "https://www.cinemascomics.com/wp-content/uploads/2020/07/spider-man-batman-marve-dc-960x720.jpg",
      principalSport: "Futbol",
    },
    {
      user: "Ariel",
      profilePicture:
        "https://vignette.wikia.nocookie.net/disney/images/a/a0/Ariel-1.png/revision/latest/top-crop/width/360/height/450?cb=20170929163212&path-prefix=es",
      principalSport: "Basketball",
    },
    {
      user: "Daniel",
      profilePicture:
        "https://www.cinemascomics.com/wp-content/uploads/2016/11/Flash-liga-de-la-justicia-justice-league-trailer-e1485419301433.jpg",
      principalSport: "Tennis",
    },
    {
      user: "Luis Alberto",
      profilePicture:
        "https://www.cinemascomics.com/wp-content/uploads/2020/07/spider-man-batman-marve-dc-960x720.jpg",
      principalSport: "Futbol",
    },
    {
      user: "Ariel",
      profilePicture:
        "https://vignette.wikia.nocookie.net/disney/images/a/a0/Ariel-1.png/revision/latest/top-crop/width/360/height/450?cb=20170929163212&path-prefix=es",
      principalSport: "Basketball",
    },
    {
      user: "Daniel",
      profilePicture:
        "https://www.cinemascomics.com/wp-content/uploads/2016/11/Flash-liga-de-la-justicia-justice-league-trailer-e1485419301433.jpg",
      principalSport: "Tennis",
    },
    {
      user: "Luis Alberto",
      profilePicture:
        "https://www.cinemascomics.com/wp-content/uploads/2020/07/spider-man-batman-marve-dc-960x720.jpg",
      principalSport: "Futbol",
    },
    {
      user: "Ariel",
      profilePicture:
        "https://vignette.wikia.nocookie.net/disney/images/a/a0/Ariel-1.png/revision/latest/top-crop/width/360/height/450?cb=20170929163212&path-prefix=es",
      principalSport: "Basketball",
    },
    {
      user: "Daniel",
      profilePicture:
        "https://www.cinemascomics.com/wp-content/uploads/2016/11/Flash-liga-de-la-justicia-justice-league-trailer-e1485419301433.jpg",
      principalSport: "Tennis",
    },
  ];

  return (
    <ScrollView style={styles.viewContainer}>
      <View style={{ alignItems: "center" }}>
        <Searchbar placeholder={"Search"} style={styles.searchBar} />
      </View>
      {data.map((userInfo, index) => (
        <List.Item
          title={userInfo.user}
          key={index}
          description={userInfo.principalSport}
          left={() => (
            <Avatar.Image size={55} source={{ uri: userInfo.profilePicture }} />
          )}
        />
      ))}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  viewContainer: {
    marginLeft: 8,
    marginRight: 8,
  },
  btnContainer: {
    height: 35,
    width: "30%",
    marginTop: 10,

    justifyContent: "center",
  },
  followBtn: {
    backgroundColor: "#c89143",
  },
  searchBar: {
    marginBottom: 20,
    marginTop: 15,
    width: "95%",
  },
});
