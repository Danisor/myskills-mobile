import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { useTheme } from "@react-navigation/native";

export default function Nutrition() {
  const theme = useTheme();
  return (
    <View>
      <Text style={{ color: theme.colors.text }}>nutrition...</Text>
    </View>
  );
}

const styles = StyleSheet.create({});
