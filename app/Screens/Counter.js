import React from "react";
import { StyleSheet, Text, View, StatusBar } from "react-native";
import { useTheme } from "@react-navigation/native";

export default function Counter() {
  const theme = useTheme();
  return (
    <View>
      {/* Ajust StatusBar acord the theme StatusBar is the section of the time and status of the phone  */}
      <StatusBar
        barStyle={theme.dark ? "light-content" : "dark-content"}
        backgroundColor={theme.colors.card}
      />

      <Text style={{ color: theme.colors.text }}>El contador...</Text>
    </View>
  );
}

const styles = StyleSheet.create({});
