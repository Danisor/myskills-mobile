import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { useTheme } from "@react-navigation/native";

export default function Account() {
  const theme = useTheme();
  return (
    <View style={styles.view}>
      <Text style={{ color: theme.colors.text }}>cuenta</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
