import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { useTheme } from "@react-navigation/native";

export default function Injures() {
  const theme = useTheme();
  return (
    <View>
      <Text style={{ color: theme.colors.text }}>injures...</Text>
    </View>
  );
}

const styles = StyleSheet.create({});
