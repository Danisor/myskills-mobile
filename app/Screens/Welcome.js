import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";

export default function Welcome(props) {
  const { navigation } = props;
  return (
    <View style={styles.welcome}>
      <Text>Welcome...</Text>
      <Text style={{ marginBottom: 20 }}>aqui puede ir una bienbenida</Text>
      <Button title={"Unetenos"} onPress={() => navigation.navigate("login")} />
    </View>
  );
}

const styles = StyleSheet.create({
  welcome: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
